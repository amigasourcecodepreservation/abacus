# Abacus

**Description**:  Abacus is a one or two player board game. 
It is played on a hexagonal board and runs in Workbench.

  - **Technology stack**: C++ (Maxon C++/HiSoft C++), MUI, Amiga OS Classic
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/diskmaster2/abacus/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Dependencies

* TO-DO...

## Installation

* TO-DO... 


## Configuration

TO-DO

## Usage

TO-DO

## How to test the software

TO-DO

## Known issues

TO-DO

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab
https://gitlab.com/amigasourcecodepreservation

WWW
https://amigasourcepres.gitlab.io/

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

----

## License

Distributed under the terms of the GPL License version 3 or . See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/abacus/LICENSE) file for details.

----

## Credits and references

Abacus is distributed under the terms of the GNU General Public License,version 2 or later. See the LICENSE file for details.

All files part of Abacus has Copyright (C) 1997-2018 Kai Nickel



/*
* This file is part of Abacus.
* Copyright (C) 1997 Kai Nickel
* 
* Abacus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Abacus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Abacus.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifndef INCLUDE_SYSTEM_HPP
#define INCLUDE_SYSTEM_HPP
/****************************************************************************************
	System.hpp
-----------------------------------------------------------------------------------------

  Gemeinsame Systemschnittstellen

-----------------------------------------------------------------------------------------
	27.12.1996
****************************************************************************************/

extern char* version_string;
extern char* version_number;
extern char* version_date;

#include <clib/alib_protos.h>
#include <clib/utility_protos.h>

#include <pragma/utility_lib.h>

#include <stdlib.h>

#endif

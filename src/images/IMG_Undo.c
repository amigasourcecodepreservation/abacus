/*
* This file is part of Abacus.
* Copyright (C) 1997 Kai Nickel
* 
* Abacus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Abacus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Abacus.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifdef USE_IMG_UNDO_COLORS
const ULONG IMG_Undo_colors[24] =
{
	0x92929292,0x92929292,0x92929292,
	0x00000000,0x00000000,0x00000000,
	0xefefefef,0xefefefef,0xefefefef,
	0x31313131,0x61616161,0xa2a2a2a2,
	0x71717171,0x71717171,0x71717171,
	0xa2a2a2a2,0xa2a2a2a2,0xa2a2a2a2,
	0xa2a2a2a2,0x92929292,0x71717171,
	0xefefefef,0xa2a2a2a2,0x92929292,
};
#endif

//Width 17 Height 16 Depth 3 - converting...
#define IMG_UNDO_WIDTH        17
#define IMG_UNDO_HEIGHT       16
#define IMG_UNDO_DEPTH         3
#define IMG_UNDO_COMPRESSION   0
#define IMG_UNDO_MASKING       2

#ifdef USE_IMG_UNDO_HEADER
const struct BitMapHeader IMG_Undo_header =
{ 17,16,52,641,3,2,0,0,0,22,22,1024,768 };
#endif

//ifdef USE_IMG_UNDO_BODY
const UBYTE IMG_Undo_body[192] = {
0x00,0x00,0x00,0x00,0x3f,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x0f,0x80,0x00,
0x00,0x10,0x00,0x00,0x00,0x0f,0x00,0x00,0x00,0x1f,0x80,0x00,0x00,0x00,0x08,
0x00,0x00,0x1f,0x00,0x00,0x00,0x0f,0x8c,0x00,0x00,0x10,0x10,0x00,0x00,0x0f,
0x08,0x00,0x00,0x1d,0x9e,0x00,0x00,0x20,0x20,0x00,0x00,0x1f,0x1c,0x00,0x00,
0x3f,0x9f,0x00,0x00,0x40,0x20,0x00,0x00,0x3c,0x1e,0x00,0x00,0x3c,0x8f,0x00,
0x00,0x40,0x10,0x00,0x00,0x38,0x0e,0x00,0x00,0x78,0x07,0x80,0x00,0x80,0x08,
0x00,0x00,0x70,0x07,0x00,0x00,0x78,0x07,0x80,0x00,0x80,0x08,0x00,0x00,0x70,
0x07,0x00,0x00,0x78,0x07,0x80,0x00,0x80,0x08,0x00,0x00,0x70,0x07,0x00,0x00,
0x3c,0x0f,0x00,0x00,0x40,0x10,0x00,0x00,0x38,0x0e,0x00,0x00,0x3f,0x9f,0x00,
0x00,0x40,0x60,0x00,0x00,0x3c,0x1e,0x00,0x00,0x3f,0xfe,0x00,0x00,0x00,0x00,
0x00,0x00,0x1f,0xfc,0x00,0x00,0x1f,0xfc,0x00,0x00,0x00,0x00,0x00,0x00,0x0f,
0xf8,0x00,0x00,0x0f,0xf8,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0xe0,0x00,0x00,
0x03,0xe0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, };
//endif

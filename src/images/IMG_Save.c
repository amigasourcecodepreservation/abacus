/*
* This file is part of Abacus.
* Copyright (C) 1997 Kai Nickel
* 
* Abacus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Abacus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Abacus.  If not, see <http://www.gnu.org/licenses/>.
*
*/

const ULONG IMG_Save_colors[24] =
{
	0x92929292,0x92929292,0x92929292,
	0x00000000,0x00000000,0x00000000,
	0xefefefef,0xefefefef,0xefefefef,
	0x31313131,0x61616161,0xa2a2a2a2,
	0x71717171,0x71717171,0x71717171,
	0xa2a2a2a2,0xa2a2a2a2,0xa2a2a2a2,
	0xa2a2a2a2,0x92929292,0x71717171,
	0xefefefef,0xa2a2a2a2,0x92929292,
};

#define IMG_SAVE_WIDTH        17
#define IMG_SAVE_HEIGHT       16
#define IMG_SAVE_DEPTH         3
#define IMG_SAVE_COMPRESSION   0
#define IMG_SAVE_MASKING       2

const UBYTE IMG_Save_body[192] = {
0x01,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x80,0x00,0x00,0x01,0xe0,0x00,
0x00,0x03,0x40,0x00,0x00,0x01,0x00,0x00,0x00,0x02,0x70,0x00,0x00,0x07,0xa0,
0x00,0x00,0x02,0x00,0x00,0x00,0x04,0x38,0x00,0x00,0x0f,0xd0,0x00,0x00,0x04,
0x00,0x00,0x00,0x08,0x1c,0x00,0x00,0x1f,0xe8,0x00,0x00,0x08,0x00,0x00,0x00,
0x10,0x0e,0x00,0x00,0x3f,0xf4,0x00,0x00,0x10,0x00,0x00,0x00,0x20,0x0f,0x00,
0x00,0x7f,0xf6,0x00,0x00,0x20,0x00,0x00,0x00,0x00,0x1f,0x80,0x00,0xbf,0xef,
0x00,0x00,0x40,0x00,0x00,0x00,0x40,0x3f,0x80,0x00,0xdf,0xd7,0x80,0x00,0x20,
0x08,0x00,0x00,0x60,0x63,0x80,0x00,0xef,0xbf,0x00,0x00,0x10,0x00,0x00,0x00,
0x30,0xcd,0x00,0x00,0x77,0x72,0x00,0x00,0x08,0x0c,0x00,0x00,0x19,0x9c,0x00,
0x00,0x3a,0xe0,0x00,0x00,0x04,0x1e,0x00,0x00,0x0d,0x68,0x00,0x00,0x1d,0xb0,
0x00,0x00,0x02,0x0c,0x00,0x00,0x07,0x70,0x00,0x00,0x0f,0x90,0x00,0x00,0x00,
0x48,0x00,0x00,0x03,0xa0,0x00,0x00,0x07,0xc0,0x00,0x00,0x00,0x30,0x00,0x00,
0x03,0xa0,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x60,0x00,0x00, };


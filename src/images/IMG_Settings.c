/*
* This file is part of Abacus.
* Copyright (C) 1997 Kai Nickel
* 
* Abacus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Abacus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Abacus.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifdef USE_IMG_SETTINGS_COLORS
const ULONG IMG_Settings_colors[24] =
{
	0x92929292,0x92929292,0x92929292,
	0x00000000,0x00000000,0x00000000,
	0xefefefef,0xefefefef,0xefefefef,
	0x31313131,0x61616161,0xa2a2a2a2,
	0x71717171,0x71717171,0x71717171,
	0xa2a2a2a2,0xa2a2a2a2,0xa2a2a2a2,
	0xa2a2a2a2,0x92929292,0x71717171,
	0xefefefef,0xa2a2a2a2,0x92929292,
};
#endif

//Width 17 Height 16 Depth 3 - converting...
#define IMG_SETTINGS_WIDTH        17
#define IMG_SETTINGS_HEIGHT       16
#define IMG_SETTINGS_DEPTH         3
#define IMG_SETTINGS_COMPRESSION   0
#define IMG_SETTINGS_MASKING       2

#ifdef USE_IMG_SETTINGS_HEADER
const struct BitMapHeader IMG_Settings_header =
{ 17,16,52,556,3,2,0,0,0,22,22,1024,768 };
#endif

//ifdef USE_IMG_SETTINGS_BODY
const UBYTE IMG_Settings_body[192] = {
0x1f,0x00,0x00,0x00,0x1f,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7f,0xc0,0x00,
0x00,0x40,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0xc1,0x60,0x00,0x00,0xff,0x20,
0x00,0x00,0x01,0x80,0x00,0x00,0xa1,0xe0,0x00,0x00,0x61,0xa0,0x00,0x00,0x25,
0x80,0x00,0x00,0x9c,0xe0,0x00,0x00,0x51,0xa0,0x00,0x00,0x02,0x80,0x00,0x00,
0xfd,0x60,0x80,0x00,0x9b,0x3f,0x00,0x00,0x01,0x80,0x80,0x00,0x1a,0xff,0x80,
0x00,0x16,0x7f,0x00,0x00,0x03,0x00,0x00,0x00,0x15,0xff,0x80,0x00,0x0c,0xff,
0x00,0x00,0x06,0x00,0x00,0x00,0x13,0x7e,0x80,0x00,0x09,0x00,0x00,0x00,0x04,
0x81,0x00,0x00,0x16,0xfe,0x80,0x00,0x14,0x01,0x00,0x00,0x09,0xfe,0x00,0x00,
0x0f,0x54,0x80,0x00,0x02,0x01,0x00,0x00,0x01,0xfe,0x00,0x00,0x17,0xfe,0x80,
0x00,0x09,0x01,0x00,0x00,0x04,0xfe,0x00,0x00,0x1b,0x54,0x80,0x00,0x09,0x01,
0x00,0x00,0x0c,0xfe,0x00,0x00,0x0e,0xaa,0x80,0x00,0x02,0x01,0x00,0x00,0x01,
0xfe,0x00,0x00,0x04,0x00,0x80,0x00,0x0b,0xff,0x00,0x00,0x04,0x00,0x00,0x00,
0x0f,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x80,0x00, };
//endif
